var createEngine = require('voxel-engine');
var createTerrain = require('voxel-perlin-terrain');

// create the game
var game = createEngine({
  generateVoxelChunk: createTerrain({scaleFactor:6}),
  chunkDistance: 2,
  materials: ['obsidian', ['grass', 'dirt', 'grass_dirt'], 'grass', 'plank'],
  texturePath: './textures/',
  startingPosition: [35, -1200, 35],
  worldOrigin: [0, 0, 0],
  fogDisabled: true,
  lightsDisabled: false
});
var container = document.getElementById('container');
game.appendTo(container);

var createPlayer = require('voxel-player')(game);
var shama = createPlayer('./textures/shama.png');
shama.yaw.position.set(0, 0, 0);
shama.possess();

// add some trees
var createTree = require('voxel-trees');
for (var i = 0; i < 1; i++) {
    createTree({ bark: 2, leaves: 3, position: {x: 35, y:-1200, z:35}, setBlock: game.setBlock})(game);
}
